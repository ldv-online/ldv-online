#!/usr/bin/perl -w

$ENV{'WSDLADDR'} = "http://localhost:9888/ldvo";
$ENV{'LDV_HOME'} = "/home/iceberg/ldv";
$ENV{'LDV_WORKDIR'} = "/home/iceberg/tmp_client";

$rundir  = "$ENV{'LDV_WORKDIR'}/run";
$finished_dir  = "$ENV{'LDV_WORKDIR'}/run/finished";

use SOAP::Lite;

my $csd = SOAP::Lite -> service($ENV{'WSDLADDR'}.'?wsdl');


my $command = $csd->getTask(SOAP::Data-> type ("string") -> name ("arg0") -> value ("unique node"));

print $_."\n" foreach (keys %$command);

if($command->{status} eq 'OK') {
	print $command->{id}."\n";
	print $command->{parent}."\n";
	print $command->{env}."\n";
	print $command->{rule}."\n";
	print $command->{name}."\n";
	#print $command->{data}."\n";
	# save driver data to rundir
	my $driver = "$rundir/$command->{name}";
	open FH, ">$driver" or die "Can't open file $driver: $!";
	binmode FH;
	print FH $command->{data};
	close FH or warn"Can't close file with data: $!";

	#
	# run ldv-manager at once
	#
	my $ldv_args="cd $rundir && export PATH=\$PATH:$ENV{'LDV_HOME'}/bin; LDV_DEBUG=100 LDV_TASK_ID=$command->{parent} ldv-manager envs=$command->{env}.tar.bz2 drivers=$command->{name} rule_models=$command->{rule}";
	print "$ldv_args\n";
	system($ldv_args) and die"Error";

	#
	# upload pax file
	#
	my $files = findFiles($finished_dir);
	foreach (@$files) {
		my $upload_args="cd $rundir && export PATH=\$PATH:$ENV{'LDV_HOME'}/bin; LDV_DEBUG=100 LDV_TASK_ID=$command->{parent} LDVDB=ldvo LDVUSER=root LDVDBPASSWD=....... LDVDBHOST=localhost ldv_statuses=1 ldv-upload --online $_";
		print "$upload_args\n";
		system($upload_args) and die"Error";
		print("Try to remove $_");
		system("rm -fr $_") and die"$!";
	}
	system("rm -fr $driver");

} else {
	print $command->{desc}."\n";
}



sub findFiles {
        my @files = ();
        findFilesRec($_[0], \@files);
        return \@files;
};

sub findFilesRec {
        my ($dir, $files) = @_;
        opendir DIR, $dir or die"Can't open dir!";
        my @lfiles = readdir DIR;
        foreach (@lfiles) {
                /^\.$/ and next;
                /^\.\.$/ and next;
                -d "$dir/$_" and findFilesRec("$dir/$_", $files) and next;
                push @$files, "$dir/$_";
        }
        close DIR;
        return 1;
}

