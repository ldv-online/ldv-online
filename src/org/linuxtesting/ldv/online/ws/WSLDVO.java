
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.ws;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.utils.Config;

//~--- JDK imports ------------------------------------------------------------

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import org.apache.log4j.Logger;
import org.linuxtesting.ldv.online.core.LDVLogic;
import org.linuxtesting.ldv.online.core.Task;
import org.linuxtesting.ldv.online.core.WFInfo;

/**
 *
 * @author iceberg
 */
@WebService
public class WSLDVO {

    private static final Logger log = Logger.getLogger(WSLDVO.class);

    public void publish() {
        log.info("Starting WSDL LDVO service with addr \"" + Config.getWSDLAddr() + "\"...");
        Endpoint.publish(Config.getWSDLAddr() + "?wsdl", this);
        log.info("LDVO Service successfully started");
    }

    @WebMethod
    public WFInfo sendTask(byte[] data, String username, String driverName ,String[] params) {
        log.info("Send task was called by user \""+username+"\" for driver \""+driverName+"\"");
        return LDVLogic.sendTask(data, username, driverName, params);
    }

    @WebMethod
    public Task getTask(String name) {
        return LDVLogic.getTaskToNode(name);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
