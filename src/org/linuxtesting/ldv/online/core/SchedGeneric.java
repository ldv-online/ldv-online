/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.linuxtesting.ldv.online.core;

import org.apache.log4j.Logger;
import org.linuxtesting.ldv.online.db.dao.DAOAbstractFactory;
import org.linuxtesting.ldv.online.utils.Config;

/**
 *
 * @author iceberg
 */
public class SchedGeneric {

    protected static final Logger log = Logger.getLogger(SchedGeneric.class);
    protected Config config;
    protected BackupManager backupManager;
    protected MemManager memManager;
    protected static DAOAbstractFactory dao = DAOAbstractFactory.getDAOFactory();

    public void init() {
        this.backupManager = BackupManager.getInstance();
        this.memManager = MemManager.getInstance();
    }

}
