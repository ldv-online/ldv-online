/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.linuxtesting.ldv.online.core;

/**
 *
 * @author iceberg
 */
public enum Statuses {

    // Task Statuses TS_*
    TS_NOT_RELEASED,
    TS_WAIT_FOR_VERIFICATION,

    // Launch statuses LS_*

    LS_WAIT_FOR_VERIFICATION,
    LS_VERIFICATION_IN_PROGRESS,
    LS_QUEUED,
    
    // Node Statuses NS_*
    NS_W_WAIT_FOR_TASK, // значит нод существует и у него могут быть задания

    // Statuses for stats db
    queued,
    running,
    failed,
    finished
}
