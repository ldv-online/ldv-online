/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.linuxtesting.ldv.online.core;

import java.util.List;
import org.linuxtesting.ldv.online.db.pojo.Nodes;
import org.linuxtesting.ldv.online.db.pojo.Launches;

/**
 *
 * @author iceberg
 */
public class SchedSingle extends SchedGeneric implements SchedIF {

    private static int timeout_for_one = 3000;
    private static int trycount = 5;
    private static int tryMemMonitor = 0;
   // private static long oldTime = System.currentTimeMillis();

    public void run() {

        log.debug("Start scheduler SchedSingle.");
        while (true) {
            // 1. ищем номера задач RTASKS в статусе WAIT_FOR_VERIFICATION...
            List<Launches> waitingTasks = LDVLogic.getWaitForVerificationSplittedTasks();
            if (waitingTasks != null && waitingTasks.size() > 0) {
                // 2. ожидаем свободных машин для верификации
                List<Integer> idOfWaitingClients = null;
                for (int i = 0; i < trycount; i++) {
                    List<Nodes> waitingNodes = LDVLogic.getWaitingNodes();
                    if (waitingNodes != null && waitingNodes.size() > 0) {
                        // распределяем все задачи равномерно или как-нибудь по-другому, не важно
                        for (int j = 0, k = 0; j < waitingTasks.size(); j++, k++) {
                            if (k >= waitingNodes.size()) {
                                k = 0;
                            }
                            LDVLogic.setSplittedTaskToNode(waitingTasks.get(j), waitingNodes.get(k));
                        }
                        break;
                    }
                    timeout();
                }
                //}
                // занимаемся другими делами, т.е.
                // чистим испорченные записи и т.д.
            } else {
                // когда нет дела, просто таймаут..
                timeout();
            }
            // стандартный тайм-аут
            timeout();
            // чтобы соединения не засыпали в MySQL периодически будем
            // их дрегать
            if (tryMemMonitor++ > 4) {
                log.info("MEM: Free   memory in JVM: " + Runtime.getRuntime().freeMemory() + " bytes.");
                log.info("MEM: Total Memory for JVM: " + Runtime.getRuntime().totalMemory() + " bytes.");
                tryMemMonitor = 0;
            }

            // Is Hibernate check this problem?
            /*if (tryOtherCounter > 100) {
            SQLRequests.noSleep(sManager);
            tryOtherCounter = 0;
            }*/

            // TODO: Interface to backup
           /* log.trace("Old time: " + oldTime);
            log.trace("Backup interval: " + backupInterval);
            log.trace("System.currentTimeMillis()-oldTime: " + (System.currentTimeMillis() - oldTime));
            if ((System.currentTimeMillis() - oldTime) > backupInterval) {
            log.info("Time to backup");
            String backupCommand = "cd " + params.get("WorkDir") + "; " + params.get("LDVInstalledDir") + "/ldv-online/scripts/db_backup.sh";
            VClient.runCommand(params.get("WorkDir") + "/start_backup", backupCommand);
            // delete it
            oldTime = System.currentTimeMillis();
            }*/
        }

    }

    private static void timeout() {
        try {
            Thread.sleep(timeout_for_one);
        } catch (InterruptedException e) {
            log.error("Scheduler timeout interrupted !", e);
        }
    }
}
