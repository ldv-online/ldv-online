/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.linuxtesting.ldv.online.core;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.db.dao.DAOAbstractFactory;
import org.linuxtesting.ldv.online.db.pojo.Drivers;
import org.linuxtesting.ldv.online.db.pojo.Environments;
import org.linuxtesting.ldv.online.db.pojo.Nodes;
import org.linuxtesting.ldv.online.db.pojo.RuleModels;
import org.linuxtesting.ldv.online.db.pojo.Launches;
import org.linuxtesting.ldv.online.db.pojo.Tasks;
import org.linuxtesting.ldv.online.db.pojo.Toolsets;
import org.linuxtesting.ldv.online.db.pojo.Users;
import org.linuxtesting.ldv.online.utils.Operations;
import org.linuxtesting.ldv.online.ws.WSLDVO;

/**
 *
 * @author iceberg
 */
public class LDVLogic {

    private static final Logger log = Logger.getLogger(WSLDVO.class);
    private static DAOAbstractFactory dao ;

    public static WFInfo sendTask(byte[] data, String username, String driverName ,String[] params) {
        log.trace("Send task method caled.");
        // verify all parametes???
        WFInfo wfinfo = new WFInfo();
  
        if(username==null) {wfinfo.status = "FAILED"; wfinfo.desc = "Username can't be null."; return wfinfo;}
        if(username.length() == 0) {wfinfo.status = "FAILED"; wfinfo.desc = "Username length can't be zero"; return wfinfo;}
        if(driverName==null) {wfinfo.status = "FAILED"; wfinfo.desc = "Driver name can't be null."; return wfinfo;}
        if(driverName.length() == 0) {wfinfo.status = "FAILED"; wfinfo.desc = "Driver name length can't be zero"; return wfinfo;}
        if(data==null) {wfinfo.status = "FAILED"; wfinfo.desc = "Data can't be null."; return wfinfo;}
        if(data.length == 0) {wfinfo.status = "FAILED"; wfinfo.desc = "Data length can't be zero"; return wfinfo;}
        if(params==null) {wfinfo.status = "FAILED"; wfinfo.desc = "Verification params can't be null"; return wfinfo;}
        if(params.length == 0) {wfinfo.status = "FAILED"; wfinfo.desc = "Verification params length can'b be zero"; return wfinfo;}

        List<RuleModels> ruleModels = new ArrayList<RuleModels>();
        List<Environments> environments = new ArrayList<Environments>();

        for(int i=0; i<params.length; i++) {
            String[] splittedTaskParams = params[i].split(":");
            if(splittedTaskParams[0] == null) {wfinfo.status = "FAILED"; wfinfo.desc = "Verification params number "+i+" first part - kernel name can't be null."; return wfinfo;}
            if(splittedTaskParams[0].length() == 0) {wfinfo.status = "FAILED"; wfinfo.desc = "Verification params number "+i+" first part - kernel name can't be empty."; return wfinfo;}
            if(splittedTaskParams[1] == null) {wfinfo.status = "FAILED"; wfinfo.desc = "Verification params number "+i+" second part - rule name can't be null."; return wfinfo;}
            if(splittedTaskParams[1].length() == 0) {wfinfo.status = "FAILED"; wfinfo.desc = "Verification params number "+i+" second part - rule name can't be empty."; return wfinfo;}
            environments.add(new Environments(splittedTaskParams[0]));
            ruleModels.add(new RuleModels(splittedTaskParams[1]));
        }

        Session session = HibernateUtil.getSessionFactory().openSession();
        // create and save objects
        Users user = null;
        try {
            user = new Users("not_released", username, Operations.getMd5Crc(username), username+"@not_released");
        } catch (NoSuchAlgorithmException ex) {
            wfinfo.status ="FAILED";
            wfinfo.desc ="No such algorithm exception";
            return wfinfo;
        }
        user = dao.getDAOUsers().saveOrReturnExists(user, session);
        
        Tasks task = new Tasks(driverName, Statuses.TS_NOT_RELEASED.toString(), data, data.length, user.getUsername());

        Drivers driver = new Drivers(driverName);
        Toolsets toolset = new Toolsets("model-specific","current");

        List<Launches> launches = new ArrayList<Launches>();
        for(int i=0; i<params.length; i++) {
            Launches launch = new Launches(environments.get(i), ruleModels.get(i), toolset, task, driver);
            launch.setStatus(Statuses.queued.toString());
            launches.add(launch);
        }

        // save to database
        toolset = dao.getDAOToolsets().saveOrReturnExists(toolset, session);
        dao.getDAODrivers().save(driver, session);
        wfinfo.id = dao.getDAOTasks().save(task, session);

        for(int i=0; i<params.length; i++) {
            RuleModels ruleModel = dao.getDAORuleModels().saveOrReturnExists(ruleModels.get(i), session);
            Environments environment = dao.getDAOEnvironments().saveOrReturnExists(environments.get(i), session);

            Launches launch = launches.get(i);
            launch.setEnvironments(environment);
            launch.setRuleModels(ruleModel);
            launch.setToolsets(toolset);
            dao.getDAOLaunches().save(launch, session);
        }
        
        if(session!=null && session.isOpen())
            session.close();
        
        wfinfo.status = "OK";
        wfinfo.desc = "Ok";
        return wfinfo;
    }

    static List<Launches> getWaitForVerificationSplittedTasks() {
        return dao.getDAOLaunches().getWaitForVerificationTasks();
    }

    public static void init() {
        dao = DAOAbstractFactory.getDAOFactory();
    }

    static List<Nodes> getWaitingNodes() {
        return dao.getDAONodes().getWaitingNodes();
    }

    static void setSplittedTaskToNode(Launches splittedTask, Nodes node) {
        splittedTask.setStatus(Statuses.queued.toString());
        splittedTask.setLdvoStatus(Statuses.LS_QUEUED.toString());
        splittedTask.setNodes(node);
        dao.getDAOLaunches().update(splittedTask);
    }

    static void sendSplittedTaskToNode(Launches splittedTask, Nodes node, Session session) {
        splittedTask.setStatus(Statuses.running.toString());
        splittedTask.setLdvoStatus(Statuses.LS_VERIFICATION_IN_PROGRESS.toString());
        splittedTask.setNodes(node);
        dao.getDAOLaunches().update(splittedTask, session);
    }

    public static Task getTaskToNode(String name) {
        Task task = new Task();

        log.trace("Get task method caled.");

        // verify all parametes
        if(name==null) { task.status ="FAIED"; task.desc = "Node name can't be null."; return task; }
        if(name.length() == 0) { task.status ="FAIED"; task.desc = "Node name length can't be zero"; return task; }

        // 1. Register client if it not exists: (try catch use to return report about error)
        Nodes node = dao.getDAONodes().saveOrReturnExists(new Nodes(name));

        // 2. Find task for client:
        Session session = HibernateUtil.getSessionFactory().openSession();
        Launches launch = dao.getDAOLaunches().getWaitedLaunchByNodeId(node, session);
        if(launch == null) {
            task.status ="FAIED";
            task.desc = "No more tasks";
            if(session!=null && session.isOpen()) session.close();
            return task;
        }
        
        // 3. create task object for SOAP message
        task.id = launch.getId();
        task.parent = launch.getTasks().getId();
        task.env = launch.getEnvironments().getVersion();
        task.name = launch.getDrivers().getName();
        task.rule = launch.getRuleModels().getName();
        task.data = launch.getTasks().getData();
        task.desc = "Ok";
        task.status = "OK";

        sendSplittedTaskToNode(launch, node, session);

        if ((session != null) && session.isOpen()) {
                session.close();
        }

        return task;
    }

}

