
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.utils;

//~--- JDK imports ------------------------------------------------------------

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author iceberg
 */
public class Operations {
    public static int byteArrayToInt(byte[] b, int offset) {
        int value = 0;

        for (int i = 0; i < 4; i++) {
            int shift = (4 - 1 - i) * 8;

            value += (b[i + offset] & 0x000000FF) << shift;
        }

        if (value < 0) {
            return value + Integer.MAX_VALUE;
        }

        return value;
    }

    public static String getMd5Crc(String login_plus_pass) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");

        digest.update(login_plus_pass.getBytes());

        return String.valueOf(byteArrayToInt(digest.digest(), 12));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
