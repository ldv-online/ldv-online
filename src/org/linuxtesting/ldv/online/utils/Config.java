package org.linuxtesting.ldv.online.utils;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.log4j.Logger;
import org.linuxtesting.ldv.online.db.dao.DAOAbstractFactory;
import org.linuxtesting.ldv.online.db.dao.DAOAbstractFactory.Type;

public class Config {
    
    private static final Logger log = Logger.getLogger(Config.class);
    private static Config       instance;
    private static String              configFile = "src/config.xml";
    private static String              hibernateFile = "src/hibernate.cfg.xml";
    private static String              wsdlAddr;

    private static String              wsdlPort;
    private static String              wsdlHost;
    private static String              wsdlName;
    
    private static String scheduler;
    private static Integer backupInterval;
    private static String backupFolder;
    private static boolean useBackup;
    private static String hibernateConfig;

    public static Type getDBType() {
        return DAOAbstractFactory.Type.ST_DB_MYSQL;
        //throw new UnsupportedOperationException("Not yet implemented");
    }

    private Config() {}

    public static synchronized void save(final String filename) throws ConfigurationException {
        configFile = filename;
        save();
    }

    public static synchronized void init(final String filename) throws ConfigurationException {
        configFile = filename;
        init();
    }

    public static synchronized void init() throws ConfigurationException {
        log.trace("Loading config...");
        XMLConfiguration config = new XMLConfiguration(configFile);

        scheduler = config.getString("scheduler.type");
        backupInterval = config.getInt("backup.interval");
        backupFolder = config.getString("backup.folder");
        hibernateConfig = config.getString("db");
        useBackup = config.getBoolean("backup.use");

        wsdlHost = config.getString("web-service-config.host");
        wsdlPort = config.getString("web-service-config.port");
        wsdlName = config.getString("web-service-config.name");


        wsdlAddr = "http://"+wsdlHost+':'+wsdlPort+'/'+wsdlName;
        // TODO: add check for null
    }

    public synchronized static void save() throws ConfigurationException {
        XMLConfiguration config = new XMLConfiguration();

        config.setEncoding("UTF-8");
        config.setFileName(configFile);
        config.setRootElementName("ldvo-configuration");

        config.addProperty("web-service-config.host", wsdlHost);
        config.addProperty("web-service-config.port", wsdlPort);
        config.addProperty("web-service-config.name", wsdlName);

        config.addProperty("scheduler.type", scheduler);
        config.addProperty("backup.use", useBackup);
        config.addProperty("backup.interval", backupInterval);
        config.addProperty("backup.folder", backupFolder);
        config.addProperty("db", hibernateConfig);

        config.save();
    }

    public static synchronized String getWSDLAddr() {
        return wsdlAddr;
    }

    public static synchronized String getScheduler() {
        return scheduler;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
