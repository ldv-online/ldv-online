/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.linuxtesting.ldv.online.tests;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import org.linuxtesting.ldv.online.core.WFInfo;

/**
 *
 * @author iceberg
 */
@WebService
public class SOAPSandbox {


    public static void main(String[] args) {
        SOAPSandbox sandbox = new SOAPSandbox();
        Endpoint.publish("http://localhost:9888/ldvo?wsdl", sandbox);
    }

    public static int ant  = 1;

    @WebMethod
    public String arma(String one) {
        System.out.println(one);
        return "Response "+one+"n";
    }

    @WebMethod
    public WFInfo sandBox(String one, String[] two, byte[] data) {
        System.out.println(" "+ant);
        WFInfo wfinfo = new WFInfo();
        System.out.println("STAGE0");
        if(data == null) {wfinfo.status ="FAILED"; wfinfo.desc = "data is null"; return wfinfo;}
        System.out.println("STAGE1");
        if(one == null) {wfinfo.status ="FAILED"; wfinfo.desc = "one is null"; return wfinfo;}
        System.out.println("STAGE2");
        if(two == null) {wfinfo.status ="FAILED"; wfinfo.desc = "two is null"; return wfinfo;}
        System.out.println("STAGE3");
        if(two.length == 0) {wfinfo.status ="FAILED"; wfinfo.desc = "two length is zero"; return wfinfo;}
        System.out.println("STAGE4");
        for(int i=0; i<two.length; i++) System.out.println("TWO: "+two[i]);
        System.out.println("STAGE5");
        wfinfo.id = 0;
        wfinfo.status = "OK";
        wfinfo.desc = "Ok";
        return wfinfo;
    }

}
