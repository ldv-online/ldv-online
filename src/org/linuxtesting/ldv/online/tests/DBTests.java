/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.linuxtesting.ldv.online.tests;

import java.security.NoSuchAlgorithmException;
import javax.naming.ConfigurationException;
import org.apache.log4j.Logger;
import org.linuxtesting.ldv.online.core.BackupManager;
import org.linuxtesting.ldv.online.core.MemManager;
import org.linuxtesting.ldv.online.core.SchedIF;
import org.linuxtesting.ldv.online.core.LDVLogic;
import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.db.dao.DAOAbstractFactory;
import org.linuxtesting.ldv.online.utils.Config;
import org.linuxtesting.ldv.online.ws.WSLDVO;

/**
 *
 * @author iceberg
 */
public class DBTests {

    private static final Logger log = Logger.getLogger(DBTests.class);

    public static void main(String[] args) throws ConfigurationException, NoSuchAlgorithmException, org.apache.commons.configuration.ConfigurationException {

        // init config
        Config.init();
        HibernateUtil.init();
        DAOAbstractFactory.init();

        // init Task loic
        LDVLogic.init();
        
        // init databse TODO: and all instances in first time
        //HibernateUtil.getSessionFactory();

        // create BackupManager
        BackupManager.init();

        // create MemoryManager
        MemManager.init();


        // start scheduler
        try {
            Class schedulerClass = Class.forName(Config.getScheduler());
            SchedIF scheduler = (SchedIF) schedulerClass.newInstance();
            scheduler.init();
            (new Thread(scheduler)).start();
        } catch (ClassNotFoundException ex) {
            log.error("Wrong scheduler!", ex);
            System.exit(1);
        } catch (InstantiationException ex) {
            log.error("Can't instantiate scheduler!", ex);
            System.exit(1);
        } catch (IllegalAccessException ex) {
            log.error("Bad access to scheduler class!", ex);
            System.exit(1);
        }
        

        // init web service
        WSLDVO wsldvo = new WSLDVO();
        // starting web service
        wsldvo.publish();
        // Ok!

        // and now test...
        //String[] agon = {"linux:32"};
        //wsldvo.sendTask("hoihheekkkkkkkkkkkkkkho".getBytes(), "almer", "driver8_67.tar.bz2", agon);
    }
}
