package org.linuxtesting.ldv.online.db.pojo;
// Generated 29.12.2010 13:00:42 by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * Environments generated by hbm2java
 */
public class Environments  implements java.io.Serializable {


     private Integer id;
     private String version;
     private String kind;
     private Set launcheses = new HashSet(0);

    public Environments() {
    }

    public Environments(String version) {
        this.kind = "external";
        this.version = version;
    }

    public Environments(String kind, Set launcheses) {
       this.kind = kind;
       this.launcheses = launcheses;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getVersion() {
        return this.version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }
    public String getKind() {
        return this.kind;
    }
    
    public void setKind(String kind) {
        this.kind = kind;
    }
    public Set getLauncheses() {
        return this.launcheses;
    }
    
    public void setLauncheses(Set launcheses) {
        this.launcheses = launcheses;
    }




}


