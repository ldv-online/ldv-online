package org.linuxtesting.ldv.online.db.pojo;
// Generated 29.12.2010 13:00:42 by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * Drivers generated by hbm2java
 */
public class Drivers  implements java.io.Serializable {


     private Integer id;
     private String name;
     private String origin;
     private Set launcheses = new HashSet(0);
     private Set scenarioses = new HashSet(0);

    public Drivers() {
    }

    public Drivers(String driverName) {
        this.name = driverName;
        this.origin = "external";
    }

    public Drivers(String name, String origin) {
        this.name = name;
        this.origin = origin;
    }
    public Drivers(String name, String origin, Set launcheses, Set scenarioses) {
       this.name = name;
       this.origin = origin;
       this.launcheses = launcheses;
       this.scenarioses = scenarioses;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getOrigin() {
        return this.origin;
    }
    
    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public Set getLauncheses() {
        return this.launcheses;
    }
    
    public void setLauncheses(Set launcheses) {
        this.launcheses = launcheses;
    }
    public Set getScenarioses() {
        return this.scenarioses;
    }
    
    public void setScenarioses(Set scenarioses) {
        this.scenarioses = scenarioses;
    }




}


