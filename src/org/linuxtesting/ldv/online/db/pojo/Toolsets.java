package org.linuxtesting.ldv.online.db.pojo;
// Generated 29.12.2010 13:00:42 by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * Toolsets generated by hbm2java
 */
public class Toolsets  implements java.io.Serializable {


     private Integer id;
     private String version;
     private String verifier;
     private Set launcheses = new HashSet(0);

    public Toolsets() {
    }

    public Toolsets(String verifier, String version) {
        this.verifier = verifier;
        this.version = version;
    }
	
    public Toolsets(String verifier) {
        this.verifier = verifier;
    }

    public Toolsets(String verifier, Set launcheses) {
       this.verifier = verifier;
       this.launcheses = launcheses;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getVersion() {
        return this.version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }
    public String getVerifier() {
        return this.verifier;
    }
    
    public void setVerifier(String verifier) {
        this.verifier = verifier;
    }
    public Set getLauncheses() {
        return this.launcheses;
    }
    
    public void setLauncheses(Set launcheses) {
        this.launcheses = launcheses;
    }




}


