
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.intf.DAODbPropertiesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAODriversIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOEnvironmentsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOLaunchesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAONodesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOProblemsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOProcessesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAORuleModelsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOScenariosIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOSourcesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOStatsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOTasksIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOToolsetsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOTracesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOUsersIF;
import org.linuxtesting.ldv.online.utils.Config;

/**
 *
 * @author iceberg
 */
public abstract class DAOAbstractFactory {
    protected static DAOAbstractFactory instance = null;
    protected static DAODbPropertiesIF  daoDbProperties;
    protected static DAODriversIF       daoDrivers;
    protected static DAOEnvironmentsIF  daoEnvironments;
    protected static DAOLaunchesIF      daoLaunches;
    protected static DAONodesIF         daoNodes;
    protected static DAOProblemsIF      daoProblems;
    protected static DAOProcessesIF     daoProcesses;
    protected static DAORuleModelsIF    daoRuleModels;
    protected static DAOScenariosIF     daoScenarios;
    protected static DAOSourcesIF       daoSources;
    protected static DAOStatsIF         daoStats;
    protected static DAOTasksIF         daoTasks;
    protected static DAOToolsetsIF      daoToolsets;
    protected static DAOTracesIF        daoTraces;
    protected static DAOUsersIF        daoUsers;

    public static enum Type { ST_DB_MYSQL }


    public static synchronized DAOAbstractFactory getDAOFactory() {
        if (instance == null)
            init();
        return instance;
    }

    public static synchronized void setFactoryType(Type type) {
        DAOAbstractFactory.getInstance(type);
    }

    public static void init() {
        getInstance(Config.getDBType());
    }

    private static synchronized DAOAbstractFactory getInstance(Type type) {
        if (instance == null) {
            switch (type) {
            case ST_DB_MYSQL :
                instance = new DAOFactoryMySQL().getInstance();

                break;

            default :
                instance = new DAOFactoryMySQL().getInstance();
            }
        }

        return instance;
    }

    public abstract DAODbPropertiesIF getDAODbProperties();

    public abstract DAODriversIF getDAODrivers();

    public abstract DAOEnvironmentsIF getDAOEnvironments();

    public abstract DAOLaunchesIF getDAOLaunches();

    public abstract DAOProblemsIF getDAOProblems();

    public abstract DAOProcessesIF getDAOProcesses();

    public abstract DAORuleModelsIF getDAORuleModels();

    public abstract DAOScenariosIF getDAOScenarios();

    public abstract DAOSourcesIF getDAOSources();

    public abstract DAOStatsIF getDAOStats();

    public abstract DAOTasksIF getDAOTasks();

    public abstract DAOToolsetsIF getDAOToolsets();

    public abstract DAOTracesIF getDAOTraces();

    public abstract DAONodesIF getDAONodes();

    public abstract DAOUsersIF getDAOUsers();
}


//~ Formatted by Jindent --- http://www.jindent.com
