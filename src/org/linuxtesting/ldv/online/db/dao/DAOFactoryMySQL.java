
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAODbPropertiesMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAODriversMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOEnvironmentsMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOLaunchesMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAONodesMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOProblemsMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOProcessesMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAORuleModelsMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOScenariosMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOSourcesMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOStatsMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOTasksMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOToolsetsMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOTracesMySQL;
import org.linuxtesting.ldv.online.db.dao.impl.mysql.DAOUsersMySQL;

import org.linuxtesting.ldv.online.db.dao.intf.DAODbPropertiesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAODriversIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOEnvironmentsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOLaunchesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAONodesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOProblemsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOProcessesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAORuleModelsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOScenariosIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOSourcesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOStatsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOTasksIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOToolsetsIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOTracesIF;
import org.linuxtesting.ldv.online.db.dao.intf.DAOUsersIF;

/**
 *
 * @author iceberg
 */
public class DAOFactoryMySQL extends DAOAbstractFactory {
    public synchronized DAOAbstractFactory getInstance() {
        if (instance == null) {
            instance = new DAOFactoryMySQL();
        }

        return instance;
    }

    @Override
    public DAODbPropertiesIF getDAODbProperties() {
        if (daoDbProperties == null) {
            daoDbProperties = new DAODbPropertiesMySQL();
        }

        return daoDbProperties;
    }

    @Override
    public DAODriversIF getDAODrivers() {
        if (daoDrivers == null) {
            daoDrivers = new DAODriversMySQL();
        }

        return daoDrivers;
    }

    @Override
    public DAOEnvironmentsIF getDAOEnvironments() {
        if (daoEnvironments == null) {
            daoEnvironments = new DAOEnvironmentsMySQL();
        }

        return daoEnvironments;
    }

    @Override
    public DAOLaunchesIF getDAOLaunches() {
        if (daoLaunches == null) {
            daoLaunches = new DAOLaunchesMySQL();
        }

        return daoLaunches;
    }

    @Override
    public DAOProblemsIF getDAOProblems() {
        if (daoProblems == null) {
            daoProblems = new DAOProblemsMySQL();
        }

        return daoProblems;
    }

    @Override
    public DAOProcessesIF getDAOProcesses() {
        if (daoProcesses == null) {
            daoProcesses = new DAOProcessesMySQL();
        }

        return daoProcesses;
    }

    @Override
    public DAORuleModelsIF getDAORuleModels() {
        if (daoRuleModels == null) {
            daoRuleModels = new DAORuleModelsMySQL();
        }

        return daoRuleModels;
    }

    @Override
    public DAOScenariosIF getDAOScenarios() {
        if (daoScenarios == null) {
            daoScenarios = new DAOScenariosMySQL();
        }

        return daoScenarios;
    }

    @Override
    public DAOSourcesIF getDAOSources() {
        if (daoSources == null) {
            daoSources = new DAOSourcesMySQL();
        }

        return daoSources;
    }

    @Override
    public DAOStatsIF getDAOStats() {
        if (daoStats == null) {
            daoStats = new DAOStatsMySQL();
        }

        return daoStats;
    }

    @Override
    public DAOTasksIF getDAOTasks() {
        if (daoTasks == null) {
            daoTasks = new DAOTasksMySQL();
        }

        return daoTasks;
    }

    @Override
    public DAOToolsetsIF getDAOToolsets() {
        if (daoToolsets == null) {
            daoToolsets = new DAOToolsetsMySQL();
        }

        return daoToolsets;
    }

    @Override
    public DAOTracesIF getDAOTraces() {
        if (daoTraces == null) {
            daoTraces = new DAOTracesMySQL();
        }

        return daoTraces;
    }

    @Override
    public DAONodesIF getDAONodes() {
        if (daoNodes == null) {
            daoNodes = new DAONodesMySQL();
        }

        return daoNodes;
    }

    @Override
    public DAOUsersIF getDAOUsers() {
        if (daoUsers == null) {
            daoUsers = new DAOUsersMySQL();
        }

        return daoUsers;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
