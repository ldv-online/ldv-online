
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.pojo.Problems;

/**
 *
 * @author iceberg
 */
public interface DAOProblemsIF extends DAOGenericIF<Problems, Integer> {}


//~ Formatted by Jindent --- http://www.jindent.com
