
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.pojo.Stats;

/**
 *
 * @author iceberg
 */
public interface DAOStatsIF extends DAOGenericIF<Stats, Integer> {}


//~ Formatted by Jindent --- http://www.jindent.com
