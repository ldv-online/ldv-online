
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.pojo.Scenarios;

/**
 *
 * @author iceberg
 */
public interface DAOScenariosIF extends DAOGenericIF<Scenarios, Integer> {}


//~ Formatted by Jindent --- http://www.jindent.com
