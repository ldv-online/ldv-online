
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import org.hibernate.Session;
import org.linuxtesting.ldv.online.db.pojo.RuleModels;

/**
 *
 * @author iceberg
 */
public interface DAORuleModelsIF extends DAOGenericIF<RuleModels, Integer> {

    public RuleModels saveOrReturnExists(RuleModels obj);
    public RuleModels saveOrReturnExists(RuleModels obj, Session session);

}


//~ Formatted by Jindent --- http://www.jindent.com
