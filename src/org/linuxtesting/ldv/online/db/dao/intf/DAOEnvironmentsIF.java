
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import org.hibernate.Session;
import org.linuxtesting.ldv.online.db.pojo.Environments;

/**
 *
 * @author iceberg
 */
public interface DAOEnvironmentsIF extends DAOGenericIF<Environments, Integer> {

    public Environments saveOrReturnExists(Environments obj, Session session);
    public Environments saveOrReturnExists(Environments obj);

}


//~ Formatted by Jindent --- http://www.jindent.com
