/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.linuxtesting.ldv.online.db.dao.intf;

import org.hibernate.Session;
import org.linuxtesting.ldv.online.db.pojo.Users;

/**
 *
 * @author iceberg
 */
public interface DAOUsersIF extends DAOGenericIF<Users, Integer> {

    public Users saveOrReturnExists(Users obj);
    public Users saveOrReturnExists(Users obj, Session session);
}
