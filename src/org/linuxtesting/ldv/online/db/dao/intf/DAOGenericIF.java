
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import org.hibernate.Session;

/**
 *
 * @author iceberg
 */
public interface DAOGenericIF<T extends Serializable, PK extends Serializable> {
    PK save(T newInstance);

    T read(PK id);

    void delete(T transientObject);

    void update(T newInstance);

    void saveOrUpdate(T newInstance);


    // on openned sessions
    PK save(T newInstance, Session session);

    T read(PK id, Session session);

    void delete(T transientObject, Session session);

    void update(T newInstance, Session session);

    void saveOrUpdate(T newInstance, Session session);
}


//~ Formatted by Jindent --- http://www.jindent.com
