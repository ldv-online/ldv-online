
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.pojo.Sources;

/**
 *
 * @author iceberg
 */
public interface DAOSourcesIF extends DAOGenericIF<Sources, Integer> {}


//~ Formatted by Jindent --- http://www.jindent.com
