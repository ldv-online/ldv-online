
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import org.hibernate.Session;
import org.linuxtesting.ldv.online.db.pojo.Toolsets;

/**
 *
 * @author iceberg
 */
public interface DAOToolsetsIF extends DAOGenericIF<Toolsets, Integer> {

    public Toolsets saveOrReturnExists(Toolsets toolset);
    public Toolsets saveOrReturnExists(Toolsets toolset, Session session);
}


//~ Formatted by Jindent --- http://www.jindent.com
