
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.pojo.Drivers;

/**
 *
 * @author iceberg
 */
public interface DAODriversIF extends DAOGenericIF<Drivers, Integer> {}


//~ Formatted by Jindent --- http://www.jindent.com
