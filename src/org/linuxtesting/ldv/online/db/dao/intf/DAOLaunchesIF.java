
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import java.util.List;
import org.hibernate.Session;
import org.linuxtesting.ldv.online.db.pojo.Launches;
import org.linuxtesting.ldv.online.db.pojo.Nodes;

/**
 *
 * @author iceberg
 */
public interface DAOLaunchesIF extends DAOGenericIF<Launches, Integer> {

    public List<Launches> getWaitForVerificationTasks();

    public Launches getWaitedLaunchByNodeId(Nodes node);

    public Launches getWaitedLaunchByNodeId(Nodes node, Session session);
    
}


//~ Formatted by Jindent --- http://www.jindent.com
