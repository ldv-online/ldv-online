
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.intf;

//~--- non-JDK imports --------------------------------------------------------

import java.util.List;
import org.linuxtesting.ldv.online.db.pojo.Nodes;

/**
 *
 * @author iceberg
 */
public interface DAONodesIF extends DAOGenericIF<Nodes, Integer> {

    public List<Nodes> getWaitingNodes();

    public Nodes saveOrReturnExists(Nodes obj);
    
}


//~ Formatted by Jindent --- http://www.jindent.com
