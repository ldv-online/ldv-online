
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOSourcesIF;
import org.linuxtesting.ldv.online.db.pojo.Sources;

/**
 *
 * @author iceberg
 */
public class DAOSourcesMySQL extends DAOGeneric<Sources, Integer> implements DAOSourcesIF {}


//~ Formatted by Jindent --- http://www.jindent.com
