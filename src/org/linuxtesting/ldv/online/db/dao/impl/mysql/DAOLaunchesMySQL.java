
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.linuxtesting.ldv.online.core.Statuses;
import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOLaunchesIF;
import org.linuxtesting.ldv.online.db.pojo.Launches;
import org.linuxtesting.ldv.online.db.pojo.Nodes;

/**
 *
 * @author iceberg
 */
public class DAOLaunchesMySQL extends DAOGeneric<Launches, Integer> implements DAOLaunchesIF {

    public List<Launches> getTasksByStatus(Statuses status) {
        Session session = null;
        List<Launches>  objects = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Launches.class);
            criteria.add(Restrictions.eq("ldvoStatus", status.toString()));
        
            objects = (List<Launches>) criteria.list();
        } catch (Exception e) {
            log.error("Get SplittedTasks operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return objects;
    }

    public List<Launches> getWaitForVerificationTasks() {
        return getTasksByStatus(Statuses.LS_WAIT_FOR_VERIFICATION);
    }

    public Launches getWaitedLaunchByNodeId(Nodes node) {
        Session session = null;
        List<Launches>  objects = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Launches.class);
            criteria.add(Restrictions.eq("nodes", node));
            criteria.add(Restrictions.eq("status", Statuses.queued.toString()));
            criteria.add(Restrictions.eq("ldvoStatus", Statuses.LS_QUEUED.toString()));
            objects = (List<Launches>) criteria.list();
        } catch (Exception e) {
            log.error("Get launch operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        if(objects.isEmpty()) {
            return null;
        }
        return objects.get(0);
    }

    public Launches getWaitedLaunchByNodeId(Nodes node, Session session) {
        List<Launches>  objects = null;
        try {
            Criteria criteria = session.createCriteria(Launches.class);
            criteria.add(Restrictions.eq("nodes", node));
            criteria.add(Restrictions.eq("status", Statuses.queued.toString()));
            criteria.add(Restrictions.eq("ldvoStatus", Statuses.LS_QUEUED.toString()));
            objects = (List<Launches>) criteria.list();
        } catch (Exception e) {
            log.error("Get launch operation failed!", e);
        } 
        if(objects == null || objects.isEmpty()) {
            return null;
        }
        return objects.get(0);
    }



}


//~ Formatted by Jindent --- http://www.jindent.com
