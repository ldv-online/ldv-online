
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOTasksIF;
import org.linuxtesting.ldv.online.db.pojo.Tasks;

/**
 *
 * @author iceberg
 */
public class DAOTasksMySQL extends DAOGeneric<Tasks, Integer> implements DAOTasksIF {}


//~ Formatted by Jindent --- http://www.jindent.com
