
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOScenariosIF;
import org.linuxtesting.ldv.online.db.pojo.Scenarios;

/**
 *
 * @author iceberg
 */
public class DAOScenariosMySQL extends DAOGeneric<Scenarios, Integer> implements DAOScenariosIF {}


//~ Formatted by Jindent --- http://www.jindent.com
