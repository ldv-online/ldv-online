
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOTracesIF;
import org.linuxtesting.ldv.online.db.pojo.Traces;

/**
 *
 * @author iceberg
 */
public class DAOTracesMySQL extends DAOGeneric<Traces, Integer> implements DAOTracesIF {}


//~ Formatted by Jindent --- http://www.jindent.com
