
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAODbPropertiesIF;
import org.linuxtesting.ldv.online.db.pojo.DbProperties;

/**
 *
 * @author iceberg
 */
public class DAODbPropertiesMySQL extends DAOGeneric<DbProperties, Integer> implements DAODbPropertiesIF {}


//~ Formatted by Jindent --- http://www.jindent.com
