
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOStatsIF;
import org.linuxtesting.ldv.online.db.pojo.Stats;

/**
 *
 * @author iceberg
 */
public class DAOStatsMySQL extends DAOGeneric<Stats, Integer> implements DAOStatsIF {}


//~ Formatted by Jindent --- http://www.jindent.com
