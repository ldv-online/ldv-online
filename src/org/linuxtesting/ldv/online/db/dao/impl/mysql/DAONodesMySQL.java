
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.core.Statuses;
import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAONodesIF;
import org.linuxtesting.ldv.online.db.pojo.Nodes;

/**
 *
 * @author iceberg
 */
public class DAONodesMySQL extends DAOGeneric<Nodes, Integer> implements DAONodesIF {

    public List<Nodes> getTasksByStatus(Statuses status) {
        Session session = null;
        List<Nodes>  objects = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Nodes.class);
            criteria.add(Restrictions.eq("status", status.toString()));
            objects = (List<Nodes>) criteria.list();
        } catch (Exception e) {
            log.error("Get Nodes operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return objects;
    }

    public List<Nodes> getWaitingNodes() {
        return getTasksByStatus(Statuses.NS_W_WAIT_FOR_TASK);
    }

    public Nodes saveOrReturnExists(Nodes obj) {
        Session session = null;
        Nodes retObj = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Nodes.class);
            criteria.add(Restrictions.eq("name", obj.getName()));
            List<Nodes> objects = (List<Nodes>) criteria.list();
            if (objects != null && objects.size() > 0) {
                retObj = objects.get(0);
            } else {
                session.save(obj);
                retObj = obj;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return retObj;
    }


}


//~ Formatted by Jindent --- http://www.jindent.com
