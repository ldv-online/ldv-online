
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOEnvironmentsIF;
import org.linuxtesting.ldv.online.db.pojo.Environments;

/**
 *
 * @author iceberg
 */
public class DAOEnvironmentsMySQL extends DAOGeneric<Environments, Integer> implements DAOEnvironmentsIF {

     public Environments saveOrReturnExists(Environments obj, Session session) {
        Environments  retObj  = null;

        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Environments.class);
            criteria.add(Restrictions.eq("kind", obj.getKind()));
            criteria.add(Restrictions.eq("version", obj.getVersion()));
            List<Environments> objects = (List<Environments>) criteria.list();
            if(objects!=null && objects.size()>0) {
                retObj = objects.get(0);
            } else {
                session.save(obj);
                retObj = obj;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        }
        return retObj;
    }

        public Environments saveOrReturnExists(Environments obj) {
        Session session = null;
        Environments  retObj  = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Environments.class);
            criteria.add(Restrictions.eq("kind", obj.getKind()));
            criteria.add(Restrictions.eq("version", obj.getVersion()));
            List<Environments> objects = (List<Environments>) criteria.list();
            if(objects!=null && objects.size()>0) {
                retObj = objects.get(0);
            } else {
                session.save(obj);
                retObj = obj;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return retObj;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
