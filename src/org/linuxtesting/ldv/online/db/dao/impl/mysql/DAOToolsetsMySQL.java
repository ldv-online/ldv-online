
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOToolsetsIF;
import org.linuxtesting.ldv.online.db.pojo.Toolsets;

/**
 *
 * @author iceberg
 */
public class DAOToolsetsMySQL extends DAOGeneric<Toolsets, Integer> implements DAOToolsetsIF {

     public Toolsets saveOrReturnExists(Toolsets toolset, Session session) {
       Toolsets  retObj  = null;

        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Toolsets.class);
            criteria.add(Restrictions.eq("verifier", toolset.getVerifier()));
            criteria.add(Restrictions.eq("version", toolset.getVersion()));
            List<Toolsets> objects = (List<Toolsets>) criteria.list();
            if(objects!=null && objects.size()>0) {
                retObj = objects.get(0);
            } else {
                session.save(toolset);
                retObj = toolset;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        }
        return retObj;
    }

    public Toolsets saveOrReturnExists(Toolsets toolset) {
        Session session = null;
        Toolsets  retObj  = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Toolsets.class);
            criteria.add(Restrictions.eq("verifier", toolset.getVerifier()));
            criteria.add(Restrictions.eq("version", toolset.getVersion()));
            List<Toolsets> objects = (List<Toolsets>) criteria.list();
            if(objects!=null && objects.size()>0) {
                retObj = objects.get(0);
            } else {
                session.save(toolset);
                retObj = toolset;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return retObj;
    }



}


//~ Formatted by Jindent --- http://www.jindent.com
