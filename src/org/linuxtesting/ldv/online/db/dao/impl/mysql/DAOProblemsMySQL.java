
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOProblemsIF;
import org.linuxtesting.ldv.online.db.pojo.Problems;

/**
 *
 * @author iceberg
 */
public class DAOProblemsMySQL extends DAOGeneric<Problems, Integer> implements DAOProblemsIF {}


//~ Formatted by Jindent --- http://www.jindent.com
