
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAORuleModelsIF;
import org.linuxtesting.ldv.online.db.pojo.RuleModels;

/**
 *
 * @author iceberg
 */
public class DAORuleModelsMySQL extends DAOGeneric<RuleModels, Integer> implements DAORuleModelsIF {

         public RuleModels saveOrReturnExists(RuleModels obj, Session session) {
        RuleModels  retObj  = null;

        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(RuleModels.class);
            criteria.add(Restrictions.eq("name", obj.getName()));
            List<RuleModels> objects = (List<RuleModels>) criteria.list();
            if(objects!=null && objects.size()>0) {
                retObj = objects.get(0);
            } else {
                session.save(obj);
                retObj = obj;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } 
        return retObj;
    }

     public RuleModels saveOrReturnExists(RuleModels obj) {
        Session session = null;
        RuleModels  retObj  = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(RuleModels.class);
            criteria.add(Restrictions.eq("name", obj.getName()));
            List<RuleModels> objects = (List<RuleModels>) criteria.list();
            if(objects!=null && objects.size()>0) {
                retObj = objects.get(0);
            } else {
                session.save(obj);
                retObj = obj;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return retObj;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
