
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAOUsersIF;
import org.linuxtesting.ldv.online.db.pojo.Users;

/**
 *
 * @author iceberg
 */
public class DAOUsersMySQL extends DAOGeneric<Users, Integer> implements DAOUsersIF {

    public Users saveOrReturnExists(Users obj, Session session) {
        Users  retObj  = null;

        try {
           session.beginTransaction();
            Criteria criteria = session.createCriteria(Users.class);
            criteria.add(Restrictions.eq("username", obj.getUsername()));
            criteria.add(Restrictions.eq("role", obj.getRole()));
            criteria.add(Restrictions.eq("hash", obj.getHash()));
            criteria.add(Restrictions.eq("mail", obj.getMail()));
            List<Users> objects = (List<Users>) criteria.list();
            if(objects!=null && objects.size()>0) {
                retObj = objects.get(0);
            } else {
                session.save(obj);
                retObj = obj;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } 
        return retObj;
    }

        public Users saveOrReturnExists(Users obj) {
        Session session = null;
        Users  retObj  = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Users.class);
            criteria.add(Restrictions.eq("username", obj.getUsername()));
            criteria.add(Restrictions.eq("role", obj.getRole()));
            criteria.add(Restrictions.eq("hash", obj.getHash()));
            criteria.add(Restrictions.eq("mail", obj.getMail()));
            List<Users> objects = (List<Users>) criteria.list();
            if(objects!=null && objects.size()>0) {
                retObj = objects.get(0);
            } else {
                session.save(obj);
                retObj = obj;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return retObj;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
