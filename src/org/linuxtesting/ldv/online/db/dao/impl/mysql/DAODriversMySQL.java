
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl.mysql;

//~--- non-JDK imports --------------------------------------------------------

import org.linuxtesting.ldv.online.db.dao.impl.DAOGeneric;
import org.linuxtesting.ldv.online.db.dao.intf.DAODriversIF;
import org.linuxtesting.ldv.online.db.pojo.Drivers;

/**
 *
 * @author iceberg
 */
public class DAODriversMySQL extends DAOGeneric<Drivers, Integer> implements DAODriversIF {}


//~ Formatted by Jindent --- http://www.jindent.com
