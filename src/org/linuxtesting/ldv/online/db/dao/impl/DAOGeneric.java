
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package org.linuxtesting.ldv.online.db.dao.impl;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.log4j.Logger;

import org.hibernate.Session;

import org.linuxtesting.ldv.online.db.HibernateUtil;
import org.linuxtesting.ldv.online.db.dao.intf.DAOGenericIF;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.lang.reflect.ParameterizedType;

/**
 *
 * @author iceberg
 */
public class DAOGeneric<T extends Serializable, PK extends Serializable> implements DAOGenericIF<T, PK> {
    protected static final Logger log = Logger.getLogger(DAOGeneric.class);

    public void saveOrUpdate(T newInstance, Session session) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(newInstance);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        }
    }

    public void saveOrUpdate(T newInstance) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.saveOrUpdate(newInstance);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
    }

   public void update(T newInstance, Session session) {
        try {
            session.beginTransaction();
            session.update(newInstance);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        }
    }


    public void update(T newInstance) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(newInstance);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
    }

    public PK save(T newInstance, Session session) {
       PK      retObj  = null;
       try {
            session.beginTransaction();
            retObj = (PK) session.save(newInstance);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } 
        return retObj;
    }

    public T read(PK id) {
        Session session     = null;
        T       newInstance = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Class<T> _class =
                (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

            newInstance = (T) session.get(_class, id);
        } catch (Exception e) {
            log.error("Read operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }

        return newInstance;
    }

        public T read(PK id, Session session) {
        T       newInstance = null;
        try {
            Class<T> _class =
                (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

            newInstance = (T) session.get(_class, id);
        } catch (Exception e) {
            log.error("Read operation failed!", e);
        }
        return newInstance;
    }


    public void delete(T transientObject) {
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(transientObject);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Delete operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(T transientObject, Session session) {
        try {
            session.beginTransaction();
            session.delete(transientObject);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Delete operation failed!", e);
        }
    }

    public PK save(T newInstance) {
       PK      retObj  = null;
       Session session = null;
       try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            retObj = (PK) session.save(newInstance);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Save operation failed!", e);
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return retObj;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
