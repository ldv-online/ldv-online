drop table if exists nodes;
drop table if exists users;

create table if not exists users(
	id int(10) unsigned not null auto_increment,
	role varchar(100) not null,
	username varchar(50) not null,
	hash varchar(255) not null,
	mail varchar(255) not null,

        UNIQUE (username),
        UNIQUE (mail),
	primary key(id)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

create table if not exists nodes(
	id int(10) unsigned not null auto_increment,
	name varchar(255) not null,
	status varchar(255) not null,

        UNIQUE (name),
        primary key(id)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

alter table tasks add column ldvo_status varchar(100);
alter table tasks add column data blob not null;
alter table tasks add column size int(10) not null;
alter table tasks add constraint foreign key (username) references users(username);

alter table launches add column ldvo_status varchar(100);
alter table launches add column node_id int(10) unsigned;
alter table launches add constraint foreign key (node_id) references nodes(id);